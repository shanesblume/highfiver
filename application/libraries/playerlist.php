<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class PlayerList {

	public function get_player_rank($row)
    {
    	// Get player rank from DOM
    	$rank = $row + 1; // offset $row from view
    	return $rank;
    }
    
    public function get_player_name($row, $html)
    {
    	//Get player name from DOM
    	$name = $html->find('a[href^="/team/player/27/"]');
    	return $name[$row]->plaintext;
    }

    public function get_player_team($row, $html)
    {
    	//Get player team from DOM
    	$team = $html->find('a[href^="/team/view/"]');
    	return $team[$row]->plaintext;
    }

    public function get_player_ppg($row, $html)
    {
    	//Get player ppg from DOM
    	$target_table_row = 2 + $row; // offset for other <tr> on page
    	$ppg = $html->find('tr', $target_table_row)->find('td', 7)->plaintext;
    	return $ppg;
    }

    public function get_player_link($row, $html)
    {
    	//Get player profile link on varvee.com from DOM
    	$target_table_row = 2 + $row; // offset for other <tr> on page
    	$link = $html->find('tr', $target_table_row)->find('a', 0)->href;
    	return 'http://www.varvee.com' . $link;
    }

}




