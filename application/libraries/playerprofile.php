<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class PlayerProfile {
    
    public function get_player_name($player_id, $html)
    {
    	//Get player name from DOM
    	$name = $html->find('div.profile-name');
    	return $name[0]->plaintext;
    }

    public function get_player_team($player_id, $html)
    {
    	//Get player team from DOM
    	$team = $html->find('a[href^="/organization/"]');
    	return $team[0]->plaintext;
    }

    public function get_game_date($player_id, $html)
    {
    	//Get player game date from DOM
    	foreach($html->find("/tr[@class]/td[1]") as $game_date) 
    	{
    			$game_dates[] = $game_date->plaintext;
    	}
    	return $game_dates;
    }

    public function get_game_opponent($row, $html)
    {
        //Get player game date from DOM
        foreach($html->find("/tr[@class]/td[2]") as $game_opponent) 
        {
                $game_opponents[] = $game_opponent->plaintext;
        }
        return $game_opponents;
    }


    public function get_game_result($player_id, $html)
    {
    	// Get player game results from DOM (Ex: W105-86)
    	foreach($html->find("/tr[@class]/td[3]") as $game_result) 
    	{
    		$result_info = explode('-', $game_result->plaintext); //Separates scores
    		$game_outcome = $result_info[0]; //Identifies the winning score (Ex: W105)
			if($game_outcome[0] === 'W') // Checks first character to see if target team won
			{
				$game_outcome_long[] = 'WIN';
			} else {
				$game_outcome_long[] = 'LOSS';
			};
    	}
    	return $game_outcome_long;
    }


    public function get_player_points($player_id, $html)
    {
    	//Get player points from DOM
    	foreach($html->find("/tr[@class]/td[4]") as $player_pt) 
    	{
    			$player_pts[] = $player_pt->plaintext;
    	}
    	return $player_pts;
    }

    public function get_team_points($player_id, $html)
    {
    	//Get team points from DOM
    	foreach($html->find("/tr[@class]/td[3]") as $game_result) 
    	{
    		$result_info = explode('-', $game_result->plaintext); //Separates scores
    		$game_outcome = $result_info[0]; //Identifies the winning score (Ex: W105)
			if($game_outcome[0] === 'W') // Checks first character to see if target team won
			{
				$team_pts[] = substr($result_info[0], 1); //If team won, use first score
			} else {
				$team_pts[] = substr($result_info[1], 0); //If team lost, use second score
			};
    	}
    	return $team_pts;
    }
		
}