<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Player {

	public function get_player_rank($row)
    {
    	// Get player position
    	$rank = $row + 1;
    	return $rank;
    }
    
    public function get_player_name($row)
    {
    	// Create DOM from URL
		$html = file_get_html('http://www.varvee.com/team/individual_leaderboard/54/27/sort:PointsPerGame/direction:desc/');

    	//Get player name from DOM
    	$name = $html->find('a[href^="/team/player/27/"]');
    	return $name[$row]->plaintext;
    }

    public function get_player_team($row)
    {
    	// Create DOM from URL
		$html = file_get_html('http://www.varvee.com/team/individual_leaderboard/54/27/sort:PointsPerGame/direction:desc/');

    	//Get player team from DOM
    	$team = $html->find('a[href^="/team/view/"]');
    	return $team[$row]->plaintext;
    }

    public function get_player_ppg($row)
    {
    	// Create DOM from URL
		$html = file_get_html('http://www.varvee.com/team/individual_leaderboard/54/27/sort:PointsPerGame/direction:desc/');

    	//Get player ppg from DOM
    	$target_table_row = 2 + $row;
    	$ppg = $html->find('tr', $target_table_row)->find('td', 7);
    	return $ppg;
    }

}

