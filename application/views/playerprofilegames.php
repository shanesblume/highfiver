<!-- Games List -->
<div class="col-md-12">
	<table class="table table-condensed">

		<thead>
			<tr>
				<th class="col-md-3">Date</th>
				<th class="col-md-3">Opponent</th>
				<th class="col-md-2">Result</th>
				<th class="col-md-2">Player Points</th>
				<th class="col-md-2">Team Points</th>
			</tr>
		</thead>

		<tbody>
			<?php for($i=0; $i<count($game_dates); $i++) { ?>

				<!-- Populates games into table body as rows -->
				<tr>
					<td class="col-md-3"><?php echo $game_dates[$i] ?></td>
					<td class="col-md-3"><?php echo $game_opponents[$i] ?></td>
					<td class="col-md-2"><?php echo $game_outcome_long[$i] ?></td>
					<td class="col-md-2"><?php echo $player_pts[$i] ?></td>
					<td class="col-md-2"><?php echo $team_pts[$i] ?></td>
				</tr>

			<?php } ?>
		</tbody>
	</table>
</div>
<!-- End Games List -->