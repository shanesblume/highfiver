<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>VNN HighFiver</title>

	<!-- Boostrap JS and CSS CDNs -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

</head>
<body>

	<!-- Create link to varvee.com page for scraping -->
	<?php $html = file_get_html('http://www.varvee.com/team/player/27/' . $player_id)	?>
	
	<!-- Main Container -->
	<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 well" style="margin-top:100px; ">

		<?php include('playerprofileheading.php') ?>

		<?php include('playerprofilegames.php') ?>

		<?php include('playerprofilechart.php') ?>

	</div>
	<!-- End Main Container -->

</body>
</html>