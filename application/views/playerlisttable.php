<!-- Player List -->
<div class="col-md-12">

	<table class="table table-condensed">

		<thead>
			<tr>
				<th class="col-md-2">Rank</th>
				<th class="col-md-4">Name</th>
				<th class="col-md-4">School</th>
				<th class="col-md-2">PPG</th>
			</tr>
		</thead>

		<tbody>
			<?php 

				for($i=0; $i<5; $i++) // Cycle each player, top 5
				{

					// Getting variables from libraries
					$row = $i;
					$rank = $this->playerlist->get_player_rank($row, $html);
					$name = $this->playerlist->get_player_name($row, $html);
					$team = $this->playerlist->get_player_team($row, $html);
					$ppg = $this->playerlist->get_player_ppg($row, $html);
					$link = $this->playerlist->get_player_link($row, $html);
					$link_id = explode("/", $link);
					$player_id = end($link_id);

					?>
					
					<!-- Populates players into table body as rows -->
					<tr>
						<td class="col-md-2"><?php echo $rank ?></td>
						<td class="col-md-4"><a href="<?php echo base_url('/player/index/'.$player_id) ?>"><?php echo $name ?></a></td>
						<td class="col-md-4"><?php echo $team ?></td>
						<td class="col-md-2"><?php echo $ppg ?></td>
					</tr>
					
					<?php 

				}

			?>
		</tbody>

	</table>

</div>
<!-- End Player List -->