<!-- About -->
<div class="col-md-12" style="width: $100%; height: 500px;">

	<div class="well text-center" style="font-size:1.5em; ">
		<h2>About</h2>
		<p>
		HighFiver is a web application created by Shane Blume for Varsity News Network, using the CodeIgniter PHP framework. 
		All data retreived from <a href="http://www.varvee.com/">Varvee</a>. 
		This application is a leaderboard that displays the top five players in Indiana high school boys basketball, <em>ranked by points per game</em>. 
		</p>
		<h2>Testing</h2>
		<p>
		UI testing was completed with Mozilla Firebug, Mozilla Web Developer Tools, and Chrome Web Developer; including HTML and CSS validation. Simple unit 
		testing is done using CodeIgniter's Unit Test library. Results of the unit tests can be viewed <a href="<?php echo base_url('/test') ?>">here</a>. Selenium 
		was also used during production on a limited basis (mostly so I could gain familiarity with it).
		</p>
		<h2>Source</h2>
		<p>
		The source code for this application can be found on <a href="https://shanesblume@bitbucket.org/shanesblume/highfiver.git">BitBucket</a>.
		</p>
	</div>

</div>
<!-- End About -->