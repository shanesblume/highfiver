
<?php 
	//Getting variables from libraries
	$player_name = $this->playerprofile->get_player_name($player_id, $html);
	$player_team = $this->playerprofile->get_player_team($player_id, $html);
	$game_dates = $this->playerprofile->get_game_date($player_id, $html);
	$game_outcome_long = $this->playerprofile->get_game_result($player_id, $html);
	$player_pts = $this->playerprofile->get_player_points($player_id, $html);
	$team_pts = $this->playerprofile->get_team_points($player_id, $html);
	$game_opponents = $this->playerprofile->get_game_opponent($player_id, $html);
?>

<!-- Heading -->
<div class="jumbotron">
	<p><a href="<?php echo base_url() ?>">(Return to Leaderboard)</a></p>
	<h2>VNN Presents: HighFiver</h2>
	<h3><?php echo $player_name ?> <span class="text-muted"><?php echo $player_team ?></span></h3>
</div>
<!-- End Heading -->