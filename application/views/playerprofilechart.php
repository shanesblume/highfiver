<!-- Google Chart Display -->
<div class="col-md-12" id="chart_div" style="width: 100%; height: 400px;">
</div>
<!-- End Google Chart -->

<!-- Scripts for Charting data -->
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Points Scored', '<?php echo $player_name ?>', '<?php echo $player_team ?>'],

      <?php 
	      for($i=0; $i<count($game_dates); $i++) 
	      {
	      	  // If player score is unavailable, set to 0	
	      	  if($player_pts[$i] === '-') {
	      	  	  $player_pts[$i] = '0';
	      	  } 
       		  ?>

	      	  ['<?php echo $game_dates[$i] ?>',  <?php echo $player_pts[$i] ?>,      <?php echo $team_pts[$i] ?>],

	  <?php } ?>

    ]);

    var options = {
      title: 'Points Scored by Game',
      legend: { position: 'bottom' },
      lineWidth: 3,
      pointSize: 10,
    };

    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }
</script>
<!-- End Scripts -->