<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Default controller, defined in application/config/routes.php */
class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}
}

/* Controller for using simple_html_dom.php */
class Scraper extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_html_dom()
    {
        $url = "http://www.varvee.com/team/individual_leaderboard/54/27/";
        $html = file_get_html($url);
        $data =  $html->find('.top-list-container');
        
        if(isset($data[0]))
        {
            echo $data[0]->children(1)->children(1);
        } 
    }
}