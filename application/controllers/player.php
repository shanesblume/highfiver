<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player extends CI_Controller {

	// $player_id passed from uri string to player view
	public function index($player_id)
	{
		$data['player_id'] = $player_id;

		$this->load->view('player', $data);
	}

}