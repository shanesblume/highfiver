<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

    public function index()
    {
        $this->load->library('unit_test');

        // Checking variable types passed from library->playerlist.php to view->home.php
        
        $html = file_get_html('http://www.varvee.com/team/individual_leaderboard/54/27/sort:PointsPerGame/direction:desc/');
        $row = 0;

        // PlayerList Test 1
        $test = $this->playerlist->get_player_rank($row, $html);
        $expected_result = 1;
        $test_name = 'Checks Player Rank is Valid in PlayerList';
        $this->unit->run($test, $expected_result, $test_name);   

        // PlayerList Test 2
        $test = is_string($this->playerlist->get_player_name($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Player Name is Valid in PlayerList';
        $this->unit->run($test, $expected_result, $test_name);   

        // PlayerList Test 3
        $test = is_string($this->playerlist->get_player_team($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Player Team is Valid in PlayerList';
        $this->unit->run($test, $expected_result, $test_name);   

        // PlayerList Test 4
        $test = is_string($this->playerlist->get_player_ppg($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Player PPG is Valid in PlayerList';
        $this->unit->run($test, $expected_result, $test_name);   

        // PlayerList Test 5
        $test = is_string($this->playerlist->get_player_link($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Player Link is Valid in PlayerList';
        $this->unit->run($test, $expected_result, $test_name);   


        // Checking variable types passed from library->playerprofile.php to view->player.php

        $html = file_get_html('http://www.varvee.com/team/player/27/77791'); // Must use valid link
        $player_id = 77791; // Must use valid uri segment from http://www.varvee.com/team/player/27/XXXXX

        // PlayerProfile Test 1
        $test = is_string($this->playerprofile->get_player_name($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Player Name is Valid in PlayerProfile';
        $this->unit->run($test, $expected_result, $test_name);   

        // PlayerProfile Test 2
        $test = is_string($this->playerprofile->get_player_team($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Player Team is Valid in PlayerProfile';
        $this->unit->run($test, $expected_result, $test_name);   

        // PlayerProfile Test 3
        $test = is_array($this->playerprofile->get_game_date($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Player Date is Valid in PlayerProfile';
        $this->unit->run($test, $expected_result, $test_name);   

        // PlayerProfile Test 4
        $test = is_array($this->playerprofile->get_game_opponent($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Player Opponent is Valid in PlayerProfile';
        $this->unit->run($test, $expected_result, $test_name);   

        // PlayerProfile Test 5
        $test = is_array($this->playerprofile->get_game_result($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Player Result is Valid in PlayerProfile';
        $this->unit->run($test, $expected_result, $test_name);  

        // PlayerProfile Test 6
        $test = is_array($this->playerprofile->get_player_points($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Player Points is Valid in PlayerProfile';
        $this->unit->run($test, $expected_result, $test_name);

        // PlayerProfile Test 7
        $test = is_array($this->playerprofile->get_team_points($row, $html));
        $expected_result = TRUE;
        $test_name = 'Checks Team Points is Valid in PlayerProfile';
        $this->unit->run($test, $expected_result, $test_name);

        echo $this->unit->report();
    }
}?>