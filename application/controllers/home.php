<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Default controller, defined in application/config/routes.php */
class Home extends CI_Controller {

	public function index()
	{
		$this->load->view('home');
	}

}
