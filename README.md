# README

====================================================================================

A NOTE ON MY METHODOLOGY:

To meet the 3rd party PHP framework requirement I chose to use Codeigniter to create the HighFiver app. From my understanding a few tricks are required to cleanly use a lot of OOP methods in CodeIgniter. I still wanted to use it as my framework due to my familiarity with it and the deadline given. I also chose to make functionality a priority. In hindsight I probably could have initiated a player class that implemented an interface defining it's requirements. For more functionality (like adding different sorting methods in the future) this may have been a better approach. But Varvee's site allowed me to link to a page with the players already presorted by PPG, so I decided to use that page to scrape my data rather than creating extra code to do that logic based off of the initial link provided. This still provided the information needed and kept to application fully functional and testable.

I justed wanted to make note of the reason for the limited OOP elements. Thanks you to anyone who reviews the application and any advice is appreciated!

===================================================================================

PROBLEM:

VNN wants to keep track of the leaderboard of top basketball players in Indiana.  VNN has asked you to create a web app called “High Fiver” that dynamically shows users the top five scorers in Indiana Boys Basketball, and gives users the ability to see game­by­game scoring information about any of those players in a very simple format.

Your mission is to build the High Fiver app.  Rough wireframes are attached.

USER STORIES:

1. Users should dynamically see the top 5 players in Indiana basketball, ranked based on their scoring average.

a. http://www.varvee.com/team/individual_leaderboard/54/27/

2. When a user clicks on a player, she should see a breakdown of that player's statistics by game throughout the season.  These stats include only: Points, team points, W/L

3. Users should see a graph that shows how many points the player has scored each game, plotted against how many points their team has scored in the same game. 

FUNCTIONAL REQUIREMENTS:

1. All info must be screen scraped dynamically from Varvee for every page load on High Fiver.

NON­FUNCTIONAL REQUIREMENTS:

1. Unit tests should be written for Varvee.com parsing logic when getting info

2. Application must be created in a third­party PHP web framework or Ruby on Rails

TESTING STRATEGY: 

Before releasing the High Fiver app to the world, VNN will want to ensure that it is a production­ready application. To that end, please describe your strategy for system testing (using Selenium or whatever toolset with which you’re most comfortable).

BONUS CHALLENGE: 

Build a system test around using the High Fiver app.

